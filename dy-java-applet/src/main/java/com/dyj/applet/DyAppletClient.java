package com.dyj.applet;

import com.dyj.applet.domain.*;
import com.dyj.applet.domain.query.*;
import com.dyj.applet.domain.vo.*;
import com.dyj.applet.handler.*;
import com.dyj.common.client.BaseClient;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.config.DyConfiguration;
import com.dyj.common.domain.*;
import com.dyj.common.domain.vo.*;

import java.util.List;

/**
 * @author danmo
 * @date 2024-04-16 15:28
 **/
public class DyAppletClient extends BaseClient {


    public DyAppletClient() {
    }

    public static DyAppletClient getInstance() {
        return new DyAppletClient();
    }

    public DyAppletClient tenantId(Integer tenantId) {
        super.tenantId = tenantId;
        return this;
    }

    public DyAppletClient clientKey(String clientKey) {
        this.clientKey = clientKey;
        return this;
    }


    /**
     * 通过代码获取访问令牌。
     *
     * @param code 用户授权后返回的授权码。
     * @return 返回一个包含访问令牌信息的结果对象。
     */
    public DyResult<AccessTokenVo> accessToken(String code) {
        // 使用配置信息和授权码获取访问令牌
        return new AppletTokenHandler(configuration().getAgentConfigService().loadAgentByTenantId(tenantId, clientKey)).getAccessToken(code);
    }

    /**
     * 刷新访问令牌。
     * 本方法用于根据租户ID和应用ID获取新的访问令牌。
     *
     * @return 返回一个包含刷新后的访问令牌信息的结果对象。
     */
    public DyResult<RefreshTokenVo> refreshToken(String openId) {
        DyConfiguration configuration = configuration();
        AgentConfiguration agentConfiguration = configuration.getAgentConfigService().loadAgentByTenantId(tenantId, clientKey);
        UserTokenInfo userTokenInfo = configuration.getAgentTokenService().getUserTokenInfo(agentConfiguration.getTenantId(), agentConfiguration.getClientKey(), openId);
        // 利用配置信息和授权码获取新的访问令牌
        return new AppletTokenHandler(agentConfiguration).refreshToken(userTokenInfo.getRefreshToken());
    }


    /**
     * 根据指定的租户ID和客户端ID获取客户端令牌。
     *
     * @return 返回客户端令牌的结果，包含令牌信息或其他操作结果。
     */
    public DyResult<ClientTokenVo> clientToken() {
        AgentConfiguration agentConfiguration = configuration().getAgentConfigService().loadAgentByTenantId(tenantId, clientKey);
        // 通过AccessTokenHandler处理逻辑，获取指定租户和客户端的令牌
        return new AppletTokenHandler(agentConfiguration).getClientToken();
    }


    /**
     * 带租户ID和客户端ID参数的刷新访问令牌方法。
     * 使用提供的租户ID和客户端ID刷新访问令牌。
     *
     * @return DyResult<RefreshAccessTokenVo> 包含刷新后的访问令牌信息的结果对象。
     */
    public DyResult<RefreshAccessTokenVo> refreshAccessToken(String openId) {
        DyConfiguration configuration = configuration();
        AgentConfiguration agentConfiguration = configuration.getAgentConfigService().loadAgentByTenantId(tenantId, clientKey);
        UserTokenInfo userTokenInfo = configuration.getAgentTokenService().getUserTokenInfo(agentConfiguration.getTenantId(), agentConfiguration.getClientKey(), openId);
        // 通过AccessTokenHandler处理逻辑，获取指定租户和客户端的刷新令牌
        return new AppletTokenHandler(agentConfiguration).refreshAccessToken(userTokenInfo.getRefreshToken());
    }

    /**
     * BusinessToken 生成
     *
     * @param openId 用户ID
     * @param scope  用于指定Token所对应的能力，逗号分割
     * @return DyAppletResult<BizTokenVo>
     */
    public DyAppletResult<BizTokenVo> getBizToken(String openId, String scope) {
        return new AppletTokenHandler(configuration().getAgentByTenantId(tenantId, clientKey)).getBizToken(openId, scope);
    }

    /**
     * BusinessToken 生成刷新
     *
     * @param refreshToken 刷新token
     * @return DyAppletResult<BizTokenVo>
     */
    public DyAppletResult<BizTokenVo> refreshBizToken(String refreshToken) {
        return new AppletTokenHandler(configuration().getAgentByTenantId(tenantId, clientKey)).refreshBizToken(refreshToken);
    }

    /**
     * 小程序 getAccessToken
     */
    public DyAppletResult<AppsV2TokenVo> appsV2Token() {
        return new AppletTokenHandler(configuration().getAgentByTenantId(tenantId, clientKey)).appsV2Token();
    }

    /**
     * code2Session
     *
     * @param code          login 接口返回的登录凭证
     * @param anonymousCode login 接口返回的匿名登录凭证
     * @return DySimpleResult<Code2SessionVo>
     */
    public DySimpleResult<Code2SessionVo> code2Session(String code, String anonymousCode) {
        return new LoginHandler(configuration().getAgentConfigService().loadAgentByTenantId(tenantId, clientKey)).code2Session(code, anonymousCode);
    }

    /**
     * 生成schema
     *
     * @param query 入参
     * @return DySimpleResult<GenerateSchemaVo>
     */
    public DySimpleResult<GenerateSchemaVo> generateSchema(GenerateSchemaQuery query) {
        return new SchemaHandler(configuration().getAgentConfigService().loadAgentByTenantId(tenantId, clientKey)).generateSchema(query);
    }

    /**
     * 查询schema
     *
     * @param appId  小程序ID
     * @param schema schema
     * @return DySimpleResult<QuerySchemaVo>
     */
    public DySimpleResult<QuerySchemaVo> querySchema(String appId, String schema) {
        return new SchemaHandler(configuration().getAgentConfigService().loadAgentByTenantId(tenantId, clientKey)).querySchema(appId, schema);
    }

    /**
     * 查询schema配额
     *
     * @param appId 小程序ID
     * @return DySimpleResult<QuerySchemaQuotaVo>
     */
    public DySimpleResult<QuerySchemaQuotaVo> querySchemaQuota(String appId) {
        return new SchemaHandler(configuration().getAgentConfigService().loadAgentByTenantId(tenantId, clientKey)).querySchemaQuota(appId);
    }

    /**
     * 生成urlLink
     *
     * @param query 入参
     * @return DySimpleResult<GenerateUrlLinkVo>
     */
    public DySimpleResult<GenerateUrlLinkVo> generateUrlLink(GenerateUrlLinkQuery query) {
        return new SchemaHandler(configuration().getAgentConfigService().loadAgentByTenantId(tenantId, clientKey)).generateUrlLink(query);
    }

    /**
     * 查询urlLink配额
     *
     * @param appId 小程序ID
     * @return DySimpleResult<QueryUrlLinkQuotaVo>
     */
    public DySimpleResult<QueryUrlLinkQuotaVo> queryUrlLinkQuota(String appId) {
        return new SchemaHandler(configuration().getAgentConfigService().loadAgentByTenantId(tenantId, clientKey)).queryUrlLinkQuota(appId);
    }

    /**
     * 查询urlLink
     *
     * @param appId   小程序ID
     * @param urlLink urlLink
     * @return DySimpleResult<QueryUrlLinkVo>
     */
    public DySimpleResult<QueryUrlLinkVo> queryUrlLink(String appId, String urlLink) {
        return new SchemaHandler(configuration().getAgentConfigService().loadAgentByTenantId(tenantId, clientKey)).queryUrlLink(appId, urlLink);
    }

    /**
     * 生成二维码
     *
     * @param query 入参
     * @return DySimpleResult<QrCodeVo>
     */
    public DySimpleResult<QrCodeVo> createQrCode(CreateQrCodeQuery query) {
        return new SchemaHandler(configuration().getAgentConfigService().loadAgentByTenantId(tenantId, clientKey)).createQrCode(query);
    }

    /**
     * 发送私信消息
     *
     * @param query 入参
     * @return ChatMsgResponseVo
     */
    public SendMsgResponseVo sendMessage(SendMsgQuery query) {
        return new ChatMsgHandler(configuration().getAgentByTenantId(tenantId, clientKey)).sendMessage(query);
    }

    /**
     * 发送主动私信
     *
     * @param query 入参
     * @return DySimpleResult<AuthSendMsgVo>
     */
    public DySimpleResult<AuthSendMsgVo> authSendMsg(SendMsgQuery query) {
        return new ChatMsgHandler(configuration().getAgentByTenantId(tenantId, clientKey)).authSendMsg(query);
    }

    /**
     * 查询主动私信用户授权状态
     *
     * @param openId  用户ID
     * @param cOpenId C端用户的open_id
     * @param appId   C端用户open_id所在的小程序 可不传
     * @return DySimpleResult<ImAuthStatusVo>
     */
    public DySimpleResult<ImAuthStatusVo> queryImAuthStatus(String openId, String cOpenId, String appId) {
        return new ChatMsgHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryImAuthStatus(openId, cOpenId, appId);
    }

    /**
     * 查询授权主动私信用户
     *
     * @param openId   用户ID
     * @param pageNum  页码
     * @param pageSize 每页数量
     * @return DySimpleResult<ImAuthUserListVo>
     */
    public DySimpleResult<ImAuthUserListVo> queryAuthorizeUserList(String openId, Long pageNum, Long pageSize) {
        return new ChatMsgHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryAuthorizeUserList(openId, pageNum, pageSize);
    }

    /**
     * 私信消息撤回
     *
     * @param openId           用户ID
     * @param msgId            消息ID
     * @param conversationId   会话 ID：来源于私信 webhook，接收私信消息事件，对应 webhook 的 content 里的conversation_short_id 字段
     * @param conversationType 会话类型 1- 单聊 2- 群聊
     * @return DyResult<BaseVo>
     */
    public DyResult<BaseVo> revokeMessage(String openId, String msgId, String conversationId, Integer conversationType) {
        return new ChatMsgHandler(configuration().getAgentByTenantId(tenantId, clientKey)).revokeMessage(openId, msgId, conversationId, conversationType);
    }


    /**
     * 创建线索组件
     *
     * @param categoryId 类目id
     * @param configName 配置名称
     * @param region     区域
     * @return DySimpleResult<CreateClueComponentVo>
     */
    public DySimpleResult<CreateClueComponentVo> createClueComponent(String categoryId, String configName, String region) {
        return new ClueHandler(configuration().getAgentByTenantId(tenantId, clientKey)).createClueComponent(categoryId, configName, region);
    }

    /**
     * 创建线索组件
     *
     * @param categoryId 类目id
     * @param configName 配置名称
     * @param regionList 区域
     * @return DySimpleResult<CreateClueComponentVo>
     */
    public DySimpleResult<CreateClueComponentVo> createClueComponent(String categoryId, String configName, List<String> regionList) {
        return new ClueHandler(configuration().getAgentByTenantId(tenantId, clientKey)).createClueComponent(categoryId, configName, regionList);
    }

    /**
     * 查询线索组件
     *
     * @param pageNo   页码
     * @param pageSize 页大小
     * @return DySimpleResult<ClueComponentVo>
     */
    public DySimpleResult<ClueComponentVo> queryClueComponent(Integer pageNo, Integer pageSize) {
        return new ClueHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryClueComponent(pageNo, pageSize);
    }

    /**
     * 更新线索组件
     *
     * @param configId   配置id
     * @param categoryId 类目id
     * @param configName 配置名称
     * @param region     区域
     * @return DySimpleResult
     */
    public DySimpleResult updateClueComponent(String configId, String categoryId, String configName, String region) {
        return new ClueHandler(configuration().getAgentByTenantId(tenantId, clientKey)).updateClueComponent(configId, categoryId, configName, region);
    }

    /**
     * 更新线索组件
     *
     * @param configId   配置id
     * @param categoryId 类目id
     * @param configName 配置名称
     * @param regionList 区域
     * @return DySimpleResult
     */
    public DySimpleResult updateClueComponent(String configId, String categoryId, String configName, List<String> regionList) {
        return new ClueHandler(configuration().getAgentByTenantId(tenantId, clientKey)).updateClueComponent(configId, categoryId, configName, regionList);
    }

    /**
     * 删除线索组件
     *
     * @param configId 配置id
     * @return DySimpleResult
     */
    public DySimpleResult deleteClueComponent(String configId) {
        return new ClueHandler(configuration().getAgentByTenantId(tenantId, clientKey)).deleteClueComponent(configId);
    }

    /**
     * 查询特定视频的视频数据
     *
     * @param openId   用户ID
     * @param itemIds  item_id 数组，仅能查询 access_token 对应用户上传的视频（与video_ids字段二选一，平台优先处理item_ids）
     * @param videoIds video_id 数组，仅能查询 access_token 对应用户上传的视频（与item_ids字段二选一，平台优先处理item_ids）
     * @return DyResult<AptVideoListVo>
     */
    public DyResult<AptVideoListVo> queryVideoList(String openId, List<String> itemIds, List<String> videoIds) {
        return new AptVideoHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryVideoList(openId, itemIds, videoIds);
    }

    /**
     * 查询特定视频的视频数据(经营授权)
     *
     * @param openId   用户ID
     * @param itemIds  item_id 数组，仅能查询 access_token 对应用户上传的视频（与video_ids字段二选一，平台优先处理item_ids）
     * @param videoIds video_id 数组，仅能查询 access_token 对应用户上传的视频（与item_ids字段二选一，平台优先处理item_ids）
     * @return DyResult<AptVideoListVo>
     */
    public DyResult<AptVideoListVo> queryBizVideoList(String openId, List<String> itemIds, List<String> videoIds) {
        return new AptVideoHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryBizVideoList(openId, itemIds, videoIds);
    }


    /**
     * 获取视频点赞数据
     *
     * @param openId   用户ID
     * @param itemId   item_id
     * @param dateType 数据范围，只支持近7/15/30天
     * @return DyResult<ItemLikeInfoVo>
     */
    public DyResult<ItemLikeInfoVo> getItemLikeInfo(String openId, String itemId, Integer dateType) {
        return new AptVideoHandler(configuration().getAgentByTenantId(tenantId, clientKey)).getItemLikeInfo(openId, itemId, dateType);
    }

    /**
     * 获取视频点赞数据(经营授权)
     *
     * @param openId   用户ID
     * @param itemId   item_id
     * @param dateType 数据范围，只支持近7/15/30天
     * @return DyResult<ItemLikeInfoVo>
     */
    public DyResult<ItemLikeInfoVo> getItemBizLikeInfo(String openId, String itemId, Integer dateType) {
        return new AptVideoHandler(configuration().getAgentByTenantId(tenantId, clientKey)).getItemBizLikeInfo(openId, itemId, dateType);
    }

    /**
     * 获取视频评论数据
     *
     * @param openId   用户ID
     * @param itemId   item_id
     * @param dateType 数据范围，数据范围，近7/15天；输入7代表7天、15代表15天
     * @return DyResult<ItemCommentInfoVo>
     */
    public DyResult<ItemCommentInfoVo> getItemComment(String openId, String itemId, Integer dateType) {
        return new AptVideoHandler(configuration().getAgentByTenantId(tenantId, clientKey)).getItemComment(openId, itemId, dateType);
    }

    /**
     * 获取视频评论数据（经营授权）
     *
     * @param openId   用户ID
     * @param itemId   item_id
     * @param dateType 数据范围，数据范围，近7/15天；输入7代表7天、15代表15天
     * @return DyResult<ItemCommentInfoVo>
     */
    public DyResult<ItemCommentInfoVo> getItemBizComment(String openId, String itemId, Integer dateType) {
        return new AptVideoHandler(configuration().getAgentByTenantId(tenantId, clientKey)).getItemBizComment(openId, itemId, dateType);
    }

    /**
     * 获取视频播放数据
     *
     * @param openId   用户ID
     * @param itemId   item_id
     * @param dateType 数据范围，数据范围，近7/15天；输入7代表7天、15代表15天
     * @return DyResult<ItemPlayInfoVo>
     */
    public DyResult<ItemPlayInfoVo> getItemPlay(String openId, String itemId, Integer dateType) {
        return new AptVideoHandler(configuration().getAgentByTenantId(tenantId, clientKey)).getItemPlay(openId, itemId, dateType);
    }

    /**
     * 获取视频播放数据（经营授权）
     *
     * @param openId   用户ID
     * @param itemId   item_id
     * @param dateType 数据范围，数据范围，近7/15天；输入7代表7天、15代表15天
     * @return DyResult<ItemPlayInfoVo>
     */
    public DyResult<ItemPlayInfoVo> getItemBizPlay(String openId, String itemId, Integer dateType) {
        return new AptVideoHandler(configuration().getAgentByTenantId(tenantId, clientKey)).getItemBizPlay(openId, itemId, dateType);
    }

    /**
     * 获取视频分享数据
     *
     * @param openId   用户ID
     * @param itemId   item_id
     * @param dateType 数据范围，数据范围，近7/15天；输入7代表7天、15代表15天
     * @return DyResult<ItemShareInfoVo>
     */
    public DyResult<ItemShareInfoVo> getItemShare(String openId, String itemId, Integer dateType) {
        return new AptVideoHandler(configuration().getAgentByTenantId(tenantId, clientKey)).getItemShare(openId, itemId, dateType);
    }

    /**
     * 获取视频分享数据（经营授权）
     *
     * @param openId   用户ID
     * @param itemId   item_id
     * @param dateType 数据范围，数据范围，近7/15天；输入7代表7天、15代表15天
     * @return DyResult<ItemShareInfoVo>
     */
    public DyResult<ItemShareInfoVo> getItemBizShare(String openId, String itemId, Integer dateType) {
        return new AptVideoHandler(configuration().getAgentByTenantId(tenantId, clientKey)).getItemBizShare(openId, itemId, dateType);
    }

    /**
     * 置顶评论
     *
     * @param openId    用户ID
     * @param commentId 评论ID
     * @param itemId    视频ID
     * @param top       true: 置顶, false: 取消置顶
     * @return TopCommentVo
     */
    public TopCommentVo topComment(String openId, String commentId, String itemId, Boolean top) {
        return new AptVideoHandler(configuration().getAgentByTenantId(tenantId, clientKey)).topComment(openId, commentId, itemId, top);
    }

    /**
     * 获取评论列表
     *
     * @param openId   用户ID
     * @param itemId   视频ID
     * @param sortType 列表排序方式，不传默认按推荐序，可选值：time(时间逆序)、time_asc(时间顺序)
     * @param count    每页的数量，最大不超过50，最小不低于1
     * @param cursor   分页游标
     * @return DyResult<CommentListVo>
     */
    public DyResult<CommentListVo> commentList(String openId, String itemId, String sortType, Integer count, Long cursor) {
        return new AptVideoHandler(configuration().getAgentByTenantId(tenantId, clientKey)).commentList(openId, itemId, sortType, count, cursor);
    }

    /**
     * 获取评论回复列表
     *
     * @param openId    用户ID
     * @param itemId    视频ID
     * @param commentId 评论ID
     * @param sortType  列表排序方式，不传默认按推荐序，可选值：time(时间逆序)、time_asc(时间顺序)
     * @param count     每页的数量，最大不超过50，最小不低于1
     * @param cursor    分页游标
     * @return DyResult<CommentListVo>
     */
    public DyResult<CommentListVo> commentReplyList(String openId, String itemId, String commentId, String sortType, Integer count, Long cursor) {
        return new AptVideoHandler(configuration().getAgentByTenantId(tenantId, clientKey)).commentReplyList(openId, itemId, commentId, sortType, count, cursor);
    }

    /**
     * 回复评论
     *
     * @param openId    用户ID
     * @param content   评论内容
     * @param commentId 评论ID
     * @param itemId    视频ID
     * @return DyResult<CommentReplyVo>
     */
    public DyResult<CommentReplyVo> replyComment(String openId, String content, String commentId, String itemId) {
        return new AptVideoHandler(configuration().getAgentByTenantId(tenantId, clientKey)).replyComment(openId, content, commentId, itemId);
    }

    /**
     * videoId转换itemId
     *
     * @param accessKey 字段含义：小程序id或移动网站应用id  备注：填写转化为目标应用的应用ID，如果转化为本小程序可用的itemId，填入本小程序的appid即可。
     * @param appId     小程序ID
     * @param videoIds  需要转换的videoId
     * @return DySimpleResult<ConvertResultVo>
     */
    public DySimpleResult<ConvertResultVo> videoId2itemId(String accessKey, String appId, List<String> videoIds) {
        return new AptVideoHandler(configuration().getAgentByTenantId(tenantId, clientKey)).videoId2itemId(accessKey, appId, videoIds);
    }

    /**
     * itemId转换encryptId
     *
     * @param accessKey 根据打通文档的说明，此处在使用的应用类型为小程序时应当为小程序的 appid。具体内容参见上面的 access_key 说明
     * @param itemIds   需要转换的itemId
     * @return DySimpleResult<ConvertResultVo>
     */
    public DySimpleResult<ConvertResultVo> itemId2encryptId(String accessKey, List<String> itemIds) {
        return new AptVideoHandler(configuration().getAgentByTenantId(tenantId, clientKey)).itemId2encryptId(accessKey, itemIds);
    }

    /**
     * 删除抖音搜索直达子服务
     *
     * @param subServiceId 搜索直达子服务的id（搜索直达子服务的唯一标识）
     * @return DySimpleResult
     */
    public DySimpleResult<BaseVo> deleteSearchSubService(String subServiceId) {
        return new SearchSubServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).deleteSearchSubService(subServiceId);
    }

    /**
     * 校验是否有「搜索直达服务」的创建权限
     *
     * @return DySimpleResult<CheckSearchSubVo>
     */
    public DySimpleResult<CheckSearchSubVo> checkSearchSubService() {
        return new SearchSubServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).checkSearchSubService();
    }

    /**
     * 创建抖音搜索直达子服务
     *
     * @param searchKeyWord  该子服务可用于检索的关键词
     * @param startPageUrl   对应功能服务页面的路径链接，若用户搜索到了您的小程序相应的功能服务，点击进入该服务，会跳转到您所传入的链接对应的页面中
     * @param subServiceName 搜索直达子服务的名称，例如有一个名为宝宝生成器的小程序，
     * @return DySimpleResult<BaseVo>
     */
    public DySimpleResult<BaseVo> createSearchSubService(List<String> searchKeyWord, String startPageUrl, String subServiceName) {
        return new SearchSubServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).createSearchSubService(searchKeyWord, startPageUrl, subServiceName);
    }

    /**
     * 查询抖音搜索直达子服务
     *
     * @param pageNo        查询的页号，从1开始
     * @param pageSize      查询一页的大小，从1开始
     * @param approvalState 要查询已创建服务的状态类型，0-审核中/1-已通过/2-未通过，传0就表示只查状态为审核中的服务，不传就表示查询全部状态的服务
     * @return DySimpleResult<SearchSubListVo>
     */
    public DySimpleResult<SearchSubListVo> querySearchSubService(Integer pageNo, Integer pageSize, Long approvalState) {
        return new SearchSubServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).querySearchSubService(pageNo, pageSize, approvalState);
    }

    /**
     * 创建直播任务
     *
     * @param query 入参
     * @return DyResult<CreateTaskVo>
     */
    public DyResult<CreateTaskVo> createLiveTask(CreateLiveTaskQuery query) {
        return new AptTaskHandler(configuration().getAgentByTenantId(tenantId, clientKey)).createLiveTask(query);
    }

    /**
     * 创建视频任务
     *
     * @param query 入参
     * @return DyResult<CreateTaskVo>
     */
    public DyResult<CreateTaskVo> createVideoTask(CreateVideoTaskQuery query) {
        return new AptTaskHandler(configuration().getAgentByTenantId(tenantId, clientKey)).createVideoTask(query);
    }

    /**
     * 核销直播任务
     *
     * @param openId 用户open_id,通过/oauth/access_token/获取，用户唯一标志
     * @param taskId 创建任务之后获取的任务ID
     * @return WriteOffTaskVo
     */
    public WriteOffTaskVo writeOffLiveTask(String openId, String taskId) {
        return new AptTaskHandler(configuration().getAgentByTenantId(tenantId, clientKey)).writeOffLiveTask(openId, taskId);
    }

    /**
     * 核销视频任务
     *
     * @param openId 用户open_id,通过/oauth/access_token/获取，用户唯一标志
     * @param taskId 创建任务之后获取的任务ID
     * @return WriteOffTaskVo
     */
    public WriteOffTaskVo writeOffVideoTask(String openId, String taskId) {
        return new AptTaskHandler(configuration().getAgentByTenantId(tenantId, clientKey)).writeOffVideoTask(openId, taskId);
    }

    /**
     * 注册小程序积分阈值
     *
     * @param query 入参
     * @return DySimpleResult
     */
    public DySimpleResult<BaseVo> limitOpPoint(LimitOpPointQuery query) {
        return new AptEAppletHandler(configuration().getAgentByTenantId(tenantId, clientKey)).limitOpPoint(query);
    }

    /**
     * 注册小程序预览图、定制类小程序开发者注册信息
     *
     * @param query 入参
     * @return DySimpleResult<BaseVo>
     */
    public DySimpleResult<BaseVo> registerMaApp(RegisterMaAppQuery query) {
        return new AptEAppletHandler(configuration().getAgentByTenantId(tenantId, clientKey)).registerMaApp(query);
    }

    /**
     * 查询订单的定制完成状态
     *
     * @param appId   小程序ID
     * @param openId  用户ID
     * @param orderId 订单ID
     * @return DySimpleResult<OrderCizStatusVo>
     */
    public DySimpleResult<OrderCizStatusVo> queryOrderCustomizationStatus(String appId, String openId, String orderId) {
        return new AptEAppletHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryOrderCustomizationStatus(appId, openId, orderId);
    }

    /**
     * 退会
     * 店铺会员退出
     *
     * @param appId  小程序ID
     * @param openId 用户ID
     * @param shopId 会员ID
     * @return DySimpleResult<BaseVo>
     */
    public DySimpleResult<BaseVo> shopMemberLeave(String appId, String openId, Long shopId) {
        return new AptEAppletHandler(configuration().getAgentByTenantId(tenantId, clientKey)).shopMemberLeave(appId, openId, shopId);
    }

    /**
     * 商铺同步
     *
     * @param query 入参
     * @return DyResult<SupplierSyncVo>
     */
    public DyResult<SupplierSyncVo> supplierSync(SupplierSyncQuery query) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).supplierSync(query);
    }

    /**
     * 查询店铺
     *
     * @param supplierExtId 店铺ID
     * @return DyResult<SupplierVo>
     */
    public DyResult<SupplierVo> querySupplier(String supplierExtId) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).querySupplier(supplierExtId);
    }

    /**
     * 获取抖音POI ID
     *
     * @param amapId 高德POI ID
     * @return DyResult<PoiIdVo>
     */
    public DyResult<PoiIdVo> queryPoiId(String amapId) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryPoiId(amapId);
    }


    /**
     * 店铺匹配任务结果查询
     *
     * @param supplierTaskIds 店铺任务ID
     * @return DyResult<SupplierTaskResultVo>
     */
    public DyResult<SupplierTaskResultVo> querySupplierTaskResult(String supplierTaskIds) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).querySupplierTaskResult(supplierTaskIds);
    }

    /**
     * 店铺匹配任务状态查询
     *
     * @param supplierExtId 店铺ID
     * @return DyResult<SupplierTaskStatusVo>
     */
    public DyResult<SupplierTaskStatusVo> querySupplierMatchStatus(String supplierExtId) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).querySupplierMatchStatus(supplierExtId);
    }

    /**
     * 提交门店匹配任务
     *
     * @param query 入参
     * @return DyResult<SupplierSubmitTaskVo>
     */
    public DyResult<SupplierSubmitTaskVo> submitSupplierMatchTask(SupplierSubmitTaskQuery query) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).submitSupplierMatchTask(query);
    }


    /**
     * 查询全部店铺信息接口(天级别请求5次)
     *
     * @return DyResult<SupplierTaskVo>
     */
    public DyResult<SupplierTaskVo> queryAllSupplier() {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryAllSupplier();
    }

    /**
     * 查询店铺全部信息任务返回内容
     *
     * @param taskId 任务ID
     * @return DyResult<SupplierTaskVo>
     */
    public DyResult<SupplierTaskVo> querySupplierCallback(String taskId) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).querySupplierCallback(taskId);
    }


    /**
     * （老版本）SKU同步
     *
     * @param query 入参
     * @return DyResult<BaseVo>
     */
    public DyResult<BaseVo> skuSync(SkuSyncQuery query) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).skuSync(query);
    }

    /**
     * （老版本）sku拉取(该接口由接入方实现)
     *
     * @param spuExtId  接入方SPU ID 列表
     * @param startDate 拉取价格时间区间[start_date, end_date)
     * @param endDate   拉取价格时间区间[start_date, end_date)
     * @return DyResult<SkuHotelPullVo>
     */
    public DyResult<SkuHotelPullVo> skuHotelPull(List<String> spuExtId, String startDate, String endDate) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).skuHotelPull(spuExtId, startDate, endDate);
    }

    /**
     * （老版本）多门店SPU同步
     *
     * @param query 入参
     * @return DyResult<SpuSyncVo>
     */
    public DyResult<SpuSyncVo> spuSync(SpuSyncQuery query) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).spuSync(query);
    }

    /**
     * （老版本）多门店SPU状态同步
     *
     * @param spuExtIdList 接入方商品ID列表
     * @param status       SPU状态， 1 - 在线; 2 - 下线
     * @return DyResult<SpuStatusVo>
     */
    public DyResult<SpuStatusVo> spuStatusSync(List<String> spuExtIdList, Integer status) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).spuStatusSync(spuExtIdList, status);
    }

    /**
     * （老版本）多门店SPU库存同步
     *
     * @param spuExtId 接入方商品ID
     * @param stock    库存
     * @return DyResult<SpuStockVo>
     */
    public DyResult<SpuStockVo> spuStockSync(String spuExtId, Long stock) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).spuStockSync(spuExtId, stock);
    }

    /**
     * （老版本）多门店SPU信息查询
     *
     * @param spuExtId                   第三方SPU ID
     * @param needSpuDraft               是否需要商品草稿数据(带有商品的审核状态，只展示最近30天的数据，并且最多最近提交的20次内)
     * @param spuDraftCount              需要商品草稿数据的数目(0-20)，超过这个范围默认返回20个
     * @param supplierIdsForFilterReason 供应商id列表，需要商品在某供应商下的过滤状态
     * @return DyResult<SpuVo>
     */
    public DyResult<SpuVo> spuQuery(String spuExtId, Boolean needSpuDraft, Integer spuDraftCount, List<String> supplierIdsForFilterReason) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).spuQuery(spuExtId, needSpuDraft, spuDraftCount, supplierIdsForFilterReason);
    }

    /**
     * 创建/修改团购商品
     *
     * @param query 入参
     * @return DyProductResult<SaveGoodsProductVo>
     */
    public DyProductResult<SaveGoodsProductVo> saveGoodsProduct(SaveGoodsProductQuery query) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).saveGoodsProduct(query);
    }

    /**
     * 免审修改商品
     *
     * @param query 入参
     * @return DyProductResult<String>
     */
    public DyProductResult<String> freeAuditGoodsProduct(FreeAuditGoodsProductQuery query) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).freeAuditGoodsProduct(query);
    }

    /**
     * 上下架商品
     *
     * @param productId 商品ID
     * @param outId     商品外部ID
     * @param opType    操作类型  1-上线 2-下线
     * @return DyProductResult<String>
     */
    public DyProductResult<String> operateGoodsProduct(String productId, String outId, Integer opType) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).operateGoodsProduct(productId, outId, opType);
    }

    /**
     * 同步库存
     *
     * @param query 入参
     * @return DyProductResult<String>
     */
    public DyProductResult<String> syncGoodsStock(SyncGoodsStockQuery query) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).syncGoodsStock(query);
    }

    /**
     * 查询商品模板
     *
     * @param categoryId  行业类目；详细见； 商品类目表
     * @param productType 商品类型 1 : 团购套餐 3 : 预售券 4 : 日历房 5 : 门票 7 : 旅行跟拍 8 : 一日游 11 : 代金券
     * @return DyProductResult<GoodsTemplateVo>
     */
    public DyProductResult<GoodsTemplateVo> getGoodsTemplate(String categoryId, Integer productType) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).getGoodsTemplate(categoryId, productType);
    }

    /**
     * 查询商品草稿数据
     *
     * @param productIds 商品ID列表（逗号分隔）
     * @param outIds     外部商品ID列表（逗号分隔）
     * @return DyProductResult<GoodsProductDraftVo>
     */
    public DyProductResult<GoodsProductDraftVo> getGoodsProductDraft(String productIds, String outIds) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).getGoodsProductDraft(productIds, outIds);
    }

    /**
     * 查询商品线上数据
     *
     * @param productIds 商品ID列表（逗号分隔）
     * @param outIds     外部商品ID列表（逗号分隔）
     * @return DyProductResult<GoodsProductOnlineVo>
     */
    public DyProductResult<GoodsProductOnlineVo> getGoodsProductOnline(String productIds, String outIds) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).getGoodsProductOnline(productIds, outIds);
    }

    /**
     * 查询商品线上数据列表
     *
     * @param cursor 第一页不传，之后用前一次返回的next_cursor传入进行翻页
     * @param count  分页数量，不传默认为5
     * @param status 过滤在线状态 1-在线 2-下线 3-封禁
     * @return DyProductResult
     */
    public DyProductResult<GoodsProductOnlineVo> queryGoodsProductOnlineList(String cursor, Integer count, Integer status) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryGoodsProductOnlineList(cursor, count, status);
    }

    /**
     * 查询商品草稿数据列表
     *
     * @param cursor 第一页不传，之后用前一次返回的next_cursor传入进行翻页
     * @param count  分页数量，不传默认为5
     * @param status 过滤草稿状态，10-审核中 12-审核失败 1-审核通过
     * @return DyProductResult<GoodsProductDraftVo>
     */
    public DyProductResult<GoodsProductDraftVo> queryGoodsProductDraftList(String cursor, Integer count, Integer status) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryGoodsProductDraftList(cursor, count, status);
    }

    /**
     * 创建/更新多SKU商品的SKU列表
     *
     * @param query 入参
     * @return DyProductResult<String>
     */
    public DyProductResult<String> batchSaveGoodsSku(BatchSaveGoodsSkuQuery query) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).batchSaveGoodsSku(query);
    }

    /**
     * 查询商品品类
     *
     * @param categoryId 行业类目ID，返回当前id下的直系子类目信息；传0或者不传，均返回所有一级行业类目
     * @param accountId  服务商的入驻商户ID/代运营的商户ID，不传时默认为服务商身份
     * @return GoodsCategoryVo
     */
    public GoodsCategoryVo getGoodsCategory(String categoryId, String accountId) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).getGoodsCategory(categoryId, accountId);
    }


    /**
     * 订单同步
     *
     * @param query 入参
     * @return DyResult<SyncOrderVo>
     */
    public DyResult<SyncOrderVo> syncOrder(SyncOrderQuery query) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).syncOrder(query);
    }

    /**
     * 获取POI基础数据
     *
     * @param poiId     抖音poi_id
     * @param beginDate 最近30天，开始日期(yyyy-MM-dd)
     * @param endDate   最近30天，结束日期(yyyy-MM-dd)
     * @return DyResult<PoiBaseDataVo>
     */
    public DyResult<PoiBaseDataVo> queryPoiBaseData(String poiId, String beginDate, String endDate) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryPoiBaseData(poiId, beginDate, endDate);
    }

    /**
     * POI用户数据
     *
     * @param poiId    抖音poi_id
     * @param dateType 近7/15/30天
     * @return DyResult<PoiUserDataVo>
     */
    public DyResult<PoiUserDataVo> queryPoiUserData(String poiId, Integer dateType) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryPoiUserData(poiId, dateType);
    }

    /**
     * POI服务基础数据
     *
     * @param poiId       抖音poi_id
     * @param serviceType 服务类型，40:民宿
     * @param beginDate   最近30天，开始日期(yyyy-MM-dd)
     * @param endDate     最近30天，结束日期(yyyy-MM-dd)
     * @return DyResult<PoiServiceBaseDataVo>
     */
    public DyResult<PoiServiceBaseDataVo> queryPoiServiceBaseData(String poiId, Integer serviceType, String beginDate, String endDate) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryPoiServiceBaseData(poiId, serviceType, beginDate, endDate);
    }

    /**
     * POI服务成交用户数据
     *
     * @param poiId       抖音poi_id
     * @param serviceType 近7/15/30天
     * @param dateType    服务类型，40:民宿
     * @return DyResult<PoiServiceTransactionUserDataVo>
     */
    public DyResult<PoiServiceTransactionUserDataVo> queryPoiServiceTransactionUserData(String poiId, Integer serviceType, Integer dateType) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryPoiServiceTransactionUserData(poiId, serviceType, dateType);
    }

    /**
     * POI热度榜
     *
     * @param billboardType 10：近30日餐饮类POI的热度TOP500；20：近30日景点类POI的热度TOP500；30：近30日住宿类POI的热度TOP500
     * @return DyResult<PoiBillboardVo>
     */
    public DyResult<PoiBillboardVo> queryPoiBillboard(String billboardType) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryPoiBillboard(billboardType);
    }

    /**
     * POI认领列表
     *
     * @param openId 通过/oauth/access_token/获取，用户唯一标志
     * @param cursor 分页游标, 第一页请求cursor是0, response中会返回下一页请求用到的cursor, 同时response还会返回has_more来表明是否有更多的数据
     * @param count  每页数量
     * @return DyResult
     */
    public DyResult<PoiClaimVo> queryPoiClaimList(String openId, String cursor, Integer count) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryPoiClaimList(openId, cursor, count);
    }

    /**
     * 通过高德POI ID获取抖音POI ID
     *
     * @param amapId 高德POI ID
     * @return DyResult<AmapPoiIdVo>
     */
    public DyResult<AmapPoiIdVo> queryAmapPoiId(String amapId) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryAmapPoiId(amapId);
    }

    /**
     * 优惠券同步
     *
     * @param query 入参
     * @return DyResult<SyncV2CouponVo>
     */
    public DyResult<SyncV2CouponVo> syncV2Coupon(SyncV2CouponQuery query) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).syncV2Coupon(query);
    }

    /**
     * 优惠券更新
     *
     * @param query 入参
     * @return DyResult<SyncV2CouponVo>
     */
    public DyResult<SyncV2CouponVo> syncV2CouponAvailable(SyncV2CouponAvailableQuery query) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).syncV2CouponAvailable(query);
    }

    /**
     * 通用佣金计划查询带货数据
     *
     * @param planIdList 通用佣金计划ID列表
     * @return DySimpleResult<CommonPlanSellDetailVo>
     */
    public DySimpleResult<CommonPlanSellDetailVo> queryCommonPlanSellDetail(List<Long> planIdList) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryCommonPlanSellDetail(planIdList);
    }

    /**
     * 通用佣金计划查询带货达人列表
     *
     * @param pageNum  分页参数：页码，从1开始计数
     * @param pageSize 分页参数：数量，1<=page_size<=100
     * @param planId   通用佣金计划ID
     * @return DySimpleResult<CommonPlanTalentVo>
     */
    public DySimpleResult<CommonPlanTalentVo> queryCommonPlanTalentList(Integer pageNum, Integer pageSize, Long planId) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryCommonPlanTalentList(pageNum, pageSize, planId);
    }

    /**
     * 通用佣金计划查询达人带货数据
     *
     * @param douyinIdList 待查询的达人抖音号列表
     * @param planId       通用佣金计划ID
     * @return DySimpleResult<CommonPlanTalentSellDetailVo>
     */
    public DySimpleResult<CommonPlanTalentSellDetailVo> queryCommonPlanTalentSellDetail(List<String> douyinIdList, Long planId) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryCommonPlanTalentSellDetail(douyinIdList, planId);
    }

    /**
     * 通用佣金计划查询达人带货详情
     *
     * @param contentType 带货场景，1-仅短视频 2-仅直播间 3-短视频和直播间
     * @param douyinId    达人抖音号
     * @param pageNum     分页参数：页码，从1开始计数
     * @param pageSize    分页参数：数量，1<=page_size<=100
     * @param planId      通用佣金计划ID
     * @return DySimpleResult<CommonPlanTalentMedialVo>
     */
    public DySimpleResult<CommonPlanTalentMedialVo> queryCommonPlanTalentMediaList(Integer contentType, String douyinId, Integer pageNum, Integer pageSize, Long planId) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryCommonPlanTalentMediaList(contentType, douyinId, pageNum, pageSize, planId);
    }

    /**
     * 查询通用佣金计划
     *
     * @param pageNo    分页参数：页码，从1开始计数
     * @param pageSize  分页参数：数量，1<=page_size<=100
     * @param spuId     上传小程序商品时返回的抖音内部商品ID
     * @param spuIdType 商品id类型 Product=1  MSpu=2
     * @return DySimpleResult<CommonPlanListVo>
     */
    public DySimpleResult<CommonPlanListVo> queryCommonPlanList(Integer pageNo, Integer pageSize, Long spuId, Integer spuIdType) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryCommonPlanList(pageNo, pageSize, spuId, spuIdType);
    }

    /**
     * 发布/修改通用佣金计划
     *
     * @param commissionRate 商品的抽佣率，万分数。 小程序商品可选值范围：100-2900具有代运营商服合作关系的商品可选值范围：100-8000，须小于或等于商家确认的佣金比例
     * @param contentType    商品支持的达人带货场景，可选以下的值：1：仅短视频2：仅直播间3：短视频和直播间
     * @param spuId          上传小程序商品时返回的抖音内部商品ID
     * @param planId         计划ID，创建计划时，plan_id不传或传0。修改现有计划时，传入待修改计划的ID
     * @return DySimpleResult<SaveCommonPlanVo>
     */
    public DySimpleResult<SaveCommonPlanVo> saveCommonPlan(Long commissionRate, Integer contentType, Long spuId, Long planId) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).saveCommonPlan(commissionRate, contentType, spuId, planId);
    }

    /**
     * 修改通用佣金计划状态
     *
     * @param statusList 待更新的计划与状态列表
     * @return DySimpleResult<UpdateCommonPlanStatusVo>
     */
    public DySimpleResult<UpdateCommonPlanStatusVo> updateCommonPlanStatus(List<UpdateCommonPlanStatus> statusList) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).updateCommonPlanStatus(statusList);
    }

    /**
     * 发布/修改直播间定向佣金计划
     *
     * @param query 入参
     * @return DySimpleResult<SaveCommonPlanVo>
     */
    public DySimpleResult<SaveCommonPlanVo> saveLivePlan(SaveLivePlanQuery query) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).saveLivePlan(query);
    }

    /**
     * 发布/修改短视频定向佣金计划
     *
     * @param query 入参
     * @return DySimpleResult<SaveCommonPlanVo>
     */
    public DySimpleResult<SaveCommonPlanVo> saveShortVideoPlan(SaveShortVideoPlanQuery query) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).saveShortVideoPlan(query);
    }

    /**
     * 取消定向佣金计划指定的达人
     *
     * @param planId   定向佣金计划ID
     * @param douyinId 达人抖音号
     * @return DySimpleResult
     */
    public DySimpleResult<String> deletePlanTalent(Long planId, String douyinId) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).deletePlanTalent(planId, douyinId);
    }

    /**
     * 查询达人的定向佣金计划带货数据
     *
     * @param douyinIdList 待查询的达人抖音号列表
     * @param planId       定向佣金计划ID
     * @return DySimpleResult<OrientedPlanTalentSellDetailVo>
     */
    public DySimpleResult<OrientedPlanTalentSellDetailVo> queryOrientedPlanTalentSellDetail(List<String> douyinIdList, Long planId) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryOrientedPlanTalentSellDetail(douyinIdList, planId);
    }

    /**
     * 通过商品 ID 查询定向佣金计划
     *
     * @param spuIdList 小程序商品 ID 列表，长度限制不超过 50 个
     * @return DySimpleResult<OrientedPlanTalentVo>
     */
    public DySimpleResult<OrientedPlanTalentVo> queryOrientedPlanList(List<Long> spuIdList) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryOrientedPlanList(spuIdList);
    }

    /**
     * 查询定向佣金计划带货汇总数据
     *
     * @param planIdList 待查询的定向佣金计划ID列表
     * @return DySimpleResult<OrientedPlanSellDetailVo>
     */
    public DySimpleResult<OrientedPlanSellDetailVo> queryOrientedPlanSellDetail(List<Long> planIdList) {
        return new LifeServiceHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryOrientedPlanSellDetail(planIdList);
    }


    /**
     * 查询用户可用券信息
     *
     * @param body 查询用户可用券信息请求值
     * @return
     */
    public DySimpleResult<QueryCouponReceiveInfoVo> queryCouponReceiveInfo(QueryCouponReceiveInfoQuery body) {
        return new PromotionCouponHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryCouponReceiveInfo(body);
    }

    /**
     * 用户撤销核销券
     *
     * @param body 用户撤销核销券请求值
     * @return
     */
    public DySimpleResult<ConsumeCouponIdListVo> batchRollbackConsumeCoupon(BatchRollbackConsumeCouponQuery body) {
        return new PromotionCouponHandler(configuration().getAgentByTenantId(tenantId, clientKey)).batchRollbackConsumeCoupon(body);
    }

    /**
     * 复访营销活动实时圈选用户
     *
     * @param body 复访营销活动实时圈选用户请求值
     * @return
     */
    public DySimpleResult<?> bindUserToSidebarActivity(BindUserToSidebarActivityQuery body) {
        return new PromotionCouponHandler(configuration().getAgentByTenantId(tenantId, clientKey)).bindUserToSidebarActivity(body);
    }

    /**
     * 用户核销券
     *
     * @param body 用户核销券请求值
     * @return
     */
    public DySimpleResult<ConsumeCouponIdListVo> batchConsumeCoupon(BatchConsumeCouponQuery body) {
        return new PromotionCouponHandler(configuration().getAgentByTenantId(tenantId, clientKey)).batchConsumeCoupon(body);
    }


    /**
     * 查询主播发券配置信息
     *
     * @param body 查询主播发券配置信息请求值
     * @return
     */
    public DySimpleResult<TalentCouponLimit> queryTalentCouponLimit(QueryTalentCouponLimitQuery body) {
        return new PromotionCouponHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryTalentCouponLimit(body);
    }


    /**
     * 修改主播发券权限状态
     *
     * @param body 修改主播发券权限状态请求值
     * @return
     */
    public DySimpleResult<?> updateTalentCouponStatus(UpdateTalentCouponStatusQuery body) {
        return new PromotionCouponHandler(configuration().getAgentByTenantId(tenantId, clientKey)).updateTalentCouponStatus(body);
    }


    /**
     * 更新主播发券库存上限
     *
     * @param body 更新主播发券库存上限请求值
     * @return
     */
    public DySimpleResult<?> updateTalentCouponStock(UpdateTalentCouponStockQuery body) {
        return new PromotionCouponHandler(configuration().getAgentByTenantId(tenantId, clientKey)).updateTalentCouponStock(body);
    }

    /**
     * 主播发券权限配置
     *
     * @param body 主播发券权限配置请求值
     * @return
     */
    public DySimpleResult<?> setTalentCouponApi(SetTalentCouponApiQuery body) {
        return new PromotionCouponHandler(configuration().getAgentByTenantId(tenantId, clientKey)).setTalentCouponApi(body);
    }


    /**
     * 创建营销活动
     *
     * @param body 创建营销活动请求值
     * @return
     */
    public DySimpleResult<CreatePromotionActivityVo> createPromotionActivityV2(PromotionActivityQuery<CreatePromotionActivityV2> body) {
        return new PromotionCouponHandler(configuration().getAgentByTenantId(tenantId, clientKey)).createPromotionActivityV2(body);
    }


    /**
     * 修改营销活动
     *
     * @param body 修改营销活动请求值
     * @return
     */
    public DySimpleResult<?> modifyPromotionActivityV2(PromotionActivityQuery<ModifyPromotionActivity> body) {
        return new PromotionCouponHandler(configuration().getAgentByTenantId(tenantId, clientKey)).modifyPromotionActivityV2(body);
    }


    /**
     * 查询营销活动
     *
     * @param body 查询营销活动请求值
     * @return
     */
    public QueryPromotionActivityV2Vo queryPromotionActivityV2(QueryPromotionActivityV2Query body) {
        return new PromotionCouponHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryPromotionActivityV2(body);
    }


    /**
     * 修改营销活动状态
     *
     * @param body 修改营销活动状态请求值
     * @return
     */
    public DySimpleResult<?> updatePromotionActivityStatusV2(UpdatePromotionActivityStatusV2Query body) {
        return new PromotionCouponHandler(configuration().getAgentByTenantId(tenantId, clientKey)).updatePromotionActivityStatusV2(body);
    }


    /**
     * 创建券模板
     * @param body 创建券模板请求值
     * @return
     */
    public DySimpleResult<CreateCouponMetaVO> createCouponMetaV2(CouponMetaQuery<CreateCouponMeta> body){
        return new PromotionCouponHandler(configuration().getAgentByTenantId(tenantId, clientKey)).createCouponMetaV2(body);
    }


    /**
     * 修改券模板
     * @param body 修改券模板请求值
     * @return
     */
    public DySimpleResult<?> modifyCouponMetaV2(CouponMetaQuery<ModifyCouponMeta> body){
        return new PromotionCouponHandler(configuration().getAgentByTenantId(tenantId, clientKey)).modifyCouponMetaV2(body);
    }


    /**
     * 查询券模板
     * @param body 查询券模板请求值
     * @return
     */
    public QueryCouponMetaVo queryCouponMetaV2(QueryCouponMetaQuery body){
        return new PromotionCouponHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryCouponMetaV2(body);
    }



    /**
     * 查询授权用户发放的活动信息
     * @param body 查询授权用户发放的活动信息请求值
     * @return
     */
    public DySimpleResult<QueryActivityMetaDetailListVo> queryActivityMetaDetailList(QueryActivityMetaDetailListQuery body){
        return new PromotionCouponHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryActivityMetaDetailList(body);
    }



    /**
     * 删除券模板
     * @param body 删除券模板请求值
     * @return
     */
    public DySimpleResult<?> cancelCouponMetaApi(CancelCouponMetaApiQuery body){
        return new PromotionCouponHandler(configuration().getAgentByTenantId(tenantId, clientKey)).cancelCouponMetaApi(body);
    }


    /**
     * 修改券模板库存
     * @param body 修改券模板库存请求值
     * @return
     */
    public DySimpleResult<?> updateCouponMetaStockApi(UpdateCouponMetaStockQuery body){
        return new PromotionCouponHandler(configuration().getAgentByTenantId(tenantId, clientKey)).updateCouponMetaStockApi(body);
    }


    /**
     * 修改券模板状态
     * @param body 修改券模板状态请求值
     * @return
     */
    public DySimpleResult<?> updateCouponMetaStatus(UpdateCouponMetaStatusQuery body){
        return new PromotionCouponHandler(configuration().getAgentByTenantId(tenantId, clientKey)).updateCouponMetaStatus(body);
    }


    /**
     * 查询券模板发放统计数据
     * @param body 查询券模板发放统计数据请求值
     * @return
     */
    public DySimpleResult<QueryCouponMetaStatisticsVo> queryCouponMetaStatistics(QueryCouponMetaStatisticsQuery body){
        return new PromotionCouponHandler(configuration().getAgentByTenantId(tenantId, clientKey)).queryCouponMetaStatistics(body);
    }


    /**
     * 查询对账单
     * @param body 查询对账单请求值
     * @return
     */
    public DySimpleResult<GetBillDownloadUrl> getBillDownloadUrl(GetBillDownloadUrlQuery body){
        return new PromotionCouponHandler(configuration().getAgentByTenantId(tenantId, clientKey)).getBillDownloadUrl(body);
    }

    /**
     * 创建开发者接口发券活动
     * @param body 创建开发者接口发券活动请求值
     * @return
     */
    public DySimpleResult<CreateDeveloperActivityActivityId>  createDeveloperActivity(CreateDeveloperActivityQuery body){
        return new PromotionCouponHandler(configuration().getAgentByTenantId(tenantId, clientKey)).createDeveloperActivity(body);
    }

    /**
     * 开发者接口发券
     * @param body 开发者接口发券请求值
     * @return
     */
    public DySimpleResult<SendCouponToDesignatedUserVo> sendCouponToDesignatedUser(SendCouponToDesignatedUserQuery body){
        return new PromotionCouponHandler(configuration().getAgentByTenantId(tenantId, clientKey)).sendCouponToDesignatedUser(body);
    }

    /**
     * 删除开发者接口发券活动
     * @param body 删除开发者接口发券活动请求值
     * @return
     */
    public DySimpleResult<?> deleteDeveloperActivity(DeleteDeveloperActivityQuery body){
        return new PromotionCouponHandler(configuration().getAgentByTenantId(tenantId, clientKey)).deleteDeveloperActivity(body);
    }
}