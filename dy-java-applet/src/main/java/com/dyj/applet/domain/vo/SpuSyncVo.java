package com.dyj.applet.domain.vo;

import com.dyj.common.domain.vo.BaseVo;

/**
 * @author danmo
 * @date 2024-04-29 15:18
 **/
public class SpuSyncVo extends BaseVo {

    /**
     * 抖音平台SPU ID
     */
    private String spu_id;

    public String getSpu_id() {
        return spu_id;
    }

    public void setSpu_id(String spu_id) {
        this.spu_id = spu_id;
    }
}
