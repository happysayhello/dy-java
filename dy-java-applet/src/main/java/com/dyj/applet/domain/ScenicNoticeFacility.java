package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-04-28 14:30
 **/
public class ScenicNoticeFacility {

    /**
     * 设施名称，三方自定义(每项不超过5个汉字)
     */
    private String name;

    public static ScenicNoticeFacilityBuilder builder() {
        return new ScenicNoticeFacilityBuilder();
    }

    public static class ScenicNoticeFacilityBuilder {
        private String name;

        ScenicNoticeFacilityBuilder() {
        }

        public ScenicNoticeFacilityBuilder name(String name) {
            this.name = name;
            return this;
        }

        public ScenicNoticeFacility build() {
            ScenicNoticeFacility scenicNoticeFacility = new ScenicNoticeFacility();
            scenicNoticeFacility.setName(name);
            return scenicNoticeFacility;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
