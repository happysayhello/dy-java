package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-06 16:31
 **/
public class DirectDiscountCoupon {

    /**
     * 	折扣金额（分为单位）
     */
    private Long reduce_cost;

    public static DirectDiscountCouponBuilder builder() {
        return new DirectDiscountCouponBuilder();
    }

    public static class DirectDiscountCouponBuilder {
        private Long reduce_cost;

        public DirectDiscountCouponBuilder() {
        }

        public DirectDiscountCouponBuilder reduce_cost(Long reduce_cost) {
            this.reduce_cost = reduce_cost;
            return this;
        }

        public DirectDiscountCoupon build() {
            DirectDiscountCoupon directDiscountCoupon = new DirectDiscountCoupon();
            directDiscountCoupon.setReduce_cost(reduce_cost);
            return directDiscountCoupon;
        }
    }

    public Long getReduce_cost() {
        return reduce_cost;
    }

    public void setReduce_cost(Long reduce_cost) {
        this.reduce_cost = reduce_cost;
    }
}
