package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.CommonPlanTalentSellDetail;

import java.util.Map;

/**
 * @author danmo
 * @date 2024-05-06 18:31
 **/
public class CommonPlanTalentSellDetailVo {

    /**
     * 达人带货数据，以达人抖音号为key，带货数据为value
     */
    private Map<String, CommonPlanTalentSellDetail> data;

    /**
     * 数据产出日期
     */
    private String date;

    public Map<String, CommonPlanTalentSellDetail> getData() {
        return data;
    }

    public void setData(Map<String, CommonPlanTalentSellDetail> data) {
        this.data = data;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
