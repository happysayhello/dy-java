package com.dyj.applet.domain.query;

import com.dyj.applet.domain.Sku;
import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

/**
 * @author danmo
 * @date 2024-04-29 14:30
 **/
public class SkuSyncQuery extends BaseQuery {

    private List<Sku> skus;

    /**
     * 外部平台SPU ID
     */
    private String spu_ext_id;

    public static SkuSyncQueryBuilder builder() {
        return new SkuSyncQueryBuilder();
    }

    public static class SkuSyncQueryBuilder {
        private List<Sku> skus;
        private String spuExtId;
        private Integer tenantId;
        private String clientKey;

        public SkuSyncQueryBuilder skus(List<Sku> skus) {
            this.skus = skus;
            return this;
        }

        public SkuSyncQueryBuilder spuExtId(String spuExtId) {
            this.spuExtId = spuExtId;
            return this;
        }

        public SkuSyncQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public SkuSyncQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public SkuSyncQuery build() {
            SkuSyncQuery skuSyncQuery = new SkuSyncQuery();
            skuSyncQuery.setSkus(skus);
            skuSyncQuery.setSpu_ext_id(spuExtId);
            skuSyncQuery.setTenantId(tenantId);
            skuSyncQuery.setClientKey(clientKey);
            return skuSyncQuery;
        }
    }

    public List<Sku> getSkus() {
        return skus;
    }

    public void setSkus(List<Sku> skus) {
        this.skus = skus;
    }

    public String getSpu_ext_id() {
        return spu_ext_id;
    }

    public void setSpu_ext_id(String spu_ext_id) {
        this.spu_ext_id = spu_ext_id;
    }
}
