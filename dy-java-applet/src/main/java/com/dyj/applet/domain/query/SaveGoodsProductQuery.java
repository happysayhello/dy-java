package com.dyj.applet.domain.query;

import com.dyj.applet.domain.ProductStruct;
import com.dyj.applet.domain.SkuStruct;
import com.dyj.common.domain.query.BaseQuery;

/**
 * @author danmo
 * @date 2024-05-06 10:48
 **/
public class SaveGoodsProductQuery extends BaseQuery {

    /**
     * 商品
     */
    private ProductStruct product;

    /**
     * 售卖单元
     */
    private SkuStruct sku;

    /**
     * 商品归属账户ID，非必传；传入时须与该商家满足商服关系；没有还没有商户ID，可以先不传
     */
    private String owner_account_id;

    public static SaveGoodsProductQueryBuilder builder() {
        return new SaveGoodsProductQueryBuilder();
    }

    public static class SaveGoodsProductQueryBuilder {
        private ProductStruct product;
        private SkuStruct sku;
        private String ownerAccountId;
        private Integer tenantId;
        private String clientKey;

        public SaveGoodsProductQueryBuilder product(ProductStruct product) {
            this.product = product;
            return this;
        }
        public SaveGoodsProductQueryBuilder sku(SkuStruct sku) {
            this.sku = sku;
            return this;
        }
        public SaveGoodsProductQueryBuilder ownerAccountId(String ownerAccountId) {
            this.ownerAccountId = ownerAccountId;
            return this;
        }
        public SaveGoodsProductQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }
        public SaveGoodsProductQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }
        public SaveGoodsProductQuery build() {
            SaveGoodsProductQuery saveGoodsProductQuery = new SaveGoodsProductQuery();
            saveGoodsProductQuery.setProduct(product);
            saveGoodsProductQuery.setSku(sku);
            saveGoodsProductQuery.setOwner_account_id(ownerAccountId);
            saveGoodsProductQuery.setTenantId(tenantId);
            saveGoodsProductQuery.setClientKey(clientKey);
            return saveGoodsProductQuery;
        }
    }

    public ProductStruct getProduct() {
        return product;
    }

    public void setProduct(ProductStruct product) {
        this.product = product;
    }

    public SkuStruct getSku() {
        return sku;
    }

    public void setSku(SkuStruct sku) {
        this.sku = sku;
    }

    public String getOwner_account_id() {
        return owner_account_id;
    }

    public void setOwner_account_id(String owner_account_id) {
        this.owner_account_id = owner_account_id;
    }
}
