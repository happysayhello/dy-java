package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.PoiClaim;
import com.dyj.common.domain.vo.BaseVo;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-06 16:00
 **/
public class PoiClaimVo extends BaseVo {

    private Boolean has_more;

    private Long total;

    private Long cursor;

    private List<PoiClaim> list;

    public Boolean getHas_more() {
        return has_more;
    }

    public void setHas_more(Boolean has_more) {
        this.has_more = has_more;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Long getCursor() {
        return cursor;
    }

    public void setCursor(Long cursor) {
        this.cursor = cursor;
    }

    public List<PoiClaim> getList() {
        return list;
    }

    public void setList(List<PoiClaim> list) {
        this.list = list;
    }
}
