package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.CommonPlanSellDetail;

import java.util.Map;

/**
 * @author danmo
 * @date 2024-05-06 17:15
 **/
public class CommonPlanSellDetailVo {

    /**
     * 计划带货信息详情，以计划ID为key，带货数据为value
     */
    private Map<String, CommonPlanSellDetail> data;

    /**
     * 数据产出日期
     */
    private String date;

    public Map<String, CommonPlanSellDetail> getData() {
        return data;
    }

    public void setData(Map<String, CommonPlanSellDetail> data) {
        this.data = data;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
