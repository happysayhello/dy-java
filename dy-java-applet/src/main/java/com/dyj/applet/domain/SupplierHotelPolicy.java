package com.dyj.applet.domain;

/**
 * 酒店政策
 * @author danmo
 * @date 2024-04-28 14:09
 **/
public class SupplierHotelPolicy {

    /**
     * 早餐政策
     */
    private SupplierHotelPolicyBreakFast breakfast;

    /**
     * 入住时间(HH:mm)
     */
    private String check_in_time;
    /**
     * 离店时间(HH:mm)
     */
    private String check_out_time;
    /**
     * 儿童政策
     */
    private String child;

    /**
     * 宠物政策
     */
    private String pet;

    public static SupplierHotelPolicyBuilder builder() {
        return new SupplierHotelPolicyBuilder();
    }

    public static class SupplierHotelPolicyBuilder {
        private SupplierHotelPolicyBreakFast breakfast;
        private String checkInTime;
        private String checkOutTime;
        private String child;
        private String pet;
        public SupplierHotelPolicyBuilder breakfast(SupplierHotelPolicyBreakFast breakfast) {
            this.breakfast = breakfast;
            return this;
        }
        public SupplierHotelPolicyBuilder checkInTime(String checkInTime) {
            this.checkInTime = checkInTime;
            return this;
        }
        public SupplierHotelPolicyBuilder checkOutTime(String checkOutTime) {
            this.checkOutTime = checkOutTime;
            return this;
        }
        public SupplierHotelPolicyBuilder child(String child) {
            this.child = child;
            return this;
        }
        public SupplierHotelPolicyBuilder pet(String pet) {
            this.pet = pet;
            return this;
        }
        public SupplierHotelPolicy build() {
            SupplierHotelPolicy supplierHotelPolicy = new SupplierHotelPolicy();
            supplierHotelPolicy.setBreakfast(breakfast);
            supplierHotelPolicy.setCheck_in_time(checkInTime);
            supplierHotelPolicy.setCheck_out_time(checkOutTime);
            supplierHotelPolicy.setChild(child);
            supplierHotelPolicy.setPet(pet);
            return supplierHotelPolicy;
        }
    }
    public SupplierHotelPolicyBreakFast getBreakfast() {
        return breakfast;
    }

    public void setBreakfast(SupplierHotelPolicyBreakFast breakfast) {
        this.breakfast = breakfast;
    }

    public String getCheck_in_time() {
        return check_in_time;
    }

    public void setCheck_in_time(String check_in_time) {
        this.check_in_time = check_in_time;
    }

    public String getCheck_out_time() {
        return check_out_time;
    }

    public void setCheck_out_time(String check_out_time) {
        this.check_out_time = check_out_time;
    }

    public String getChild() {
        return child;
    }

    public void setChild(String child) {
        this.child = child;
    }

    public String getPet() {
        return pet;
    }

    public void setPet(String pet) {
        this.pet = pet;
    }
}
