package com.dyj.applet.domain;

import java.util.List;
import java.util.Map;

/**
 * 商品
 * @author danmo
 * @date 2024-05-06 10:26
 **/
public class ProductStruct {

    /**
     * 商品Id，创建时不必填写，更新时如有 out_id 可不填写
     */
    private String product_id;
    /**
     * 外部商品id
     */
    private String out_id;
    /**
     * 商品名
     */
    private String product_name;
    /**
     * 品类全名，保存时不必填写
     */
    private String category_full_name;
    /**
     * 品类id
     */
    private Integer category_id;
    /**
     * 商品类型：
     * 1 : 团购套餐
     * 3 : 预售券
     * 4 : 日历房
     * 5 : 门票
     * 7 : 旅行跟拍
     * 8 : 一日游
     * 11 : 代金券
     * 14: x项目
     */
    private Integer product_type;
    /**
     * 业务线：
     * 1：闭环自研开发者
     * 3：直连服务商
     * 5：小程序
     */
    private Integer biz_line;
    /**
     * 商家名
     */
    private String account_name;
    /**
     * 售卖开始时间
     */
    private Long sold_start_time;
    /**
     * 售卖结束时间
     */
    private Long sold_end_time;
    /**
     * 第三方跳转链接，小程序商品必填
     */
    private String out_url;
    /**
     * 店铺列表
     */
    private List<Map<String, Object>> poi_list;
    /**
     * 商品属性 KV，填写时参考下文「attr_key_value_map 的格式」
     */
    private Map<String, Object> attr_key_value_map;

    private List<String> telephone;

    public static ProductStructBuilder builder() {
        return new ProductStructBuilder();
    }
    public static class ProductStructBuilder {
        private String productId;
        private String outId;
        private String productName;
        private String categoryFullName;
        private Integer categoryId;
        private Integer productType;
        private Integer bizLine;
        private String accountName;
        private Long soldStartTime;
        private Long soldEndTime;
        private String outUrl;
        private List<Map<String, Object>> poiList;
        private Map<String, Object> attrKeyValueMap;
        private List<String> telephone;
        public ProductStructBuilder productId(String productId) {
            this.productId = productId;
            return this;
        }
        public ProductStructBuilder outId(String outId) {
            this.outId = outId;
            return this;
        }
        public ProductStructBuilder productName(String productName) {
            this.productName = productName;
            return this;
        }
        public ProductStructBuilder categoryFullName(String categoryFullName) {
            this.categoryFullName = categoryFullName;
            return this;
        }
        public ProductStructBuilder categoryId(Integer categoryId) {
            this.categoryId = categoryId;
            return this;
        }
        public ProductStructBuilder productType(Integer productType) {
            this.productType = productType;
            return this;
        }
        public ProductStructBuilder bizLine(Integer bizLine) {
            this.bizLine = bizLine;
            return this;
        }
        public ProductStructBuilder accountName(String accountName) {
            this.accountName = accountName;
            return this;
        }
        public ProductStructBuilder soldStartTime(Long soldStartTime) {
            this.soldStartTime = soldStartTime;
            return this;
        }
        public ProductStructBuilder soldEndTime(Long soldEndTime) {
            this.soldEndTime = soldEndTime;
            return this;
        }
        public ProductStructBuilder outUrl(String outUrl) {
            this.outUrl = outUrl;
            return this;
        }
        public ProductStructBuilder poiList(List<Map<String, Object>> poiList) {
            this.poiList = poiList;
            return this;
        }
        public ProductStructBuilder attrKeyValueMap(Map<String, Object> attrKeyValueMap) {
            this.attrKeyValueMap = attrKeyValueMap;
            return this;
        }
        public ProductStructBuilder telephone(List<String> telephone) {
            this.telephone = telephone;
            return this;
        }
        public ProductStruct build() {
            ProductStruct productStruct = new ProductStruct();
            productStruct.setProduct_id(productId);
            productStruct.setOut_id(outId);
            productStruct.setProduct_name(productName);
            productStruct.setCategory_full_name(categoryFullName);
            productStruct.setCategory_id(categoryId);
            productStruct.setProduct_type(productType);
            productStruct.setBiz_line(bizLine);
            productStruct.setAccount_name(accountName);
            productStruct.setSold_start_time(soldStartTime);
            productStruct.setSold_end_time(soldEndTime);
            productStruct.setOut_url(outUrl);
            productStruct.setPoi_list(poiList);
            productStruct.setAttr_key_value_map(attrKeyValueMap);
            productStruct.setTelephone(telephone);
            return productStruct;
        }
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getOut_id() {
        return out_id;
    }

    public void setOut_id(String out_id) {
        this.out_id = out_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getCategory_full_name() {
        return category_full_name;
    }

    public void setCategory_full_name(String category_full_name) {
        this.category_full_name = category_full_name;
    }

    public Integer getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Integer category_id) {
        this.category_id = category_id;
    }

    public Integer getProduct_type() {
        return product_type;
    }

    public void setProduct_type(Integer product_type) {
        this.product_type = product_type;
    }

    public Integer getBiz_line() {
        return biz_line;
    }

    public void setBiz_line(Integer biz_line) {
        this.biz_line = biz_line;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public Long getSold_start_time() {
        return sold_start_time;
    }

    public void setSold_start_time(Long sold_start_time) {
        this.sold_start_time = sold_start_time;
    }

    public Long getSold_end_time() {
        return sold_end_time;
    }

    public void setSold_end_time(Long sold_end_time) {
        this.sold_end_time = sold_end_time;
    }

    public String getOut_url() {
        return out_url;
    }

    public void setOut_url(String out_url) {
        this.out_url = out_url;
    }

    public List<Map<String, Object>> getPoi_list() {
        return poi_list;
    }

    public void setPoi_list(List<Map<String, Object>> poi_list) {
        this.poi_list = poi_list;
    }

    public Map<String, Object> getAttr_key_value_map() {
        return attr_key_value_map;
    }

    public void setAttr_key_value_map(Map<String, Object> attr_key_value_map) {
        this.attr_key_value_map = attr_key_value_map;
    }

    public List<String> getTelephone() {
        return telephone;
    }

    public void setTelephone(List<String> telephone) {
        this.telephone = telephone;
    }
}
