package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-06 15:55
 **/
public class PoiBillboard {

    /**
     * poi名称
     */
    private String poi_name;
    /**
     * 排名
     */
    private String rank;
    /**
     * 得分
     */
    private String score;
    /**
     * 	poi id
     */
    private String poi_id;

    public String getPoi_name() {
        return poi_name;
    }

    public void setPoi_name(String poi_name) {
        this.poi_name = poi_name;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getPoi_id() {
        return poi_id;
    }

    public void setPoi_id(String poi_id) {
        this.poi_id = poi_id;
    }
}
