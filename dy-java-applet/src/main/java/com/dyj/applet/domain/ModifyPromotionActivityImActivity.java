package com.dyj.applet.domain;

/**
 * 修改营销活动-IM私域消息发放
 */
public class ModifyPromotionActivityImActivity {


    /**
     * <p style="text-align: left;line-height: 1.38;margin: 0px 0px 0px;"><span style="color: #171A1C;"><span style="font-size: 14px;" elementtiming="element-timing">发放须知（长度20以内）</span></span></p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;"><span style="color: #1C1F23;"><span style="font-size: 14px;" elementtiming="element-timing">优惠券发放时候的描述文案，仅对主播/员工展示，用户不可见，比如可向主播传递当前优惠券的稀有程度或口播建议</span></span></li></ul> 选填
     */
    private String notice_text;
    /**
     * <p style="text-align: left;line-height: 1.38;margin: 0px 0px 0px;"><span style="color: #171A1C;"><span style="font-size: 14px;" elementtiming="element-timing">营销活动生效时间（即可领取开始时间），单位秒</span></span></p><ul><li style="line-height: 1.38;"><span style="color: #1C1F23;"><span style="font-size: 14px;" elementtiming="element-timing">营销活动的声明周期必须为关联券模板生命周期的子集</span></span></li></ul> 选填
     */
    private Long valid_begin_time;
    /**
     * <p style="text-align: left;line-height: 1.38;margin: 0px 0px 0px;"><span style="color: #171A1C;"><span style="font-size: 14px;" elementtiming="element-timing">营销活动过期时间（即可领取结束时间），单位秒</span></span></p><ul><li style="line-height: 1.38;"><span style="color: #1C1F23;"><span style="font-size: 14px;" elementtiming="element-timing">营销活动的声明周期必须为关联券模板生命周期的子集</span></span></li></ul> 选填
     */
    private Long valid_end_time;

    public String getNotice_text() {
        return notice_text;
    }

    public ModifyPromotionActivityImActivity setNotice_text(String notice_text) {
        this.notice_text = notice_text;
        return this;
    }

    public Long getValid_begin_time() {
        return valid_begin_time;
    }

    public ModifyPromotionActivityImActivity setValid_begin_time(Long valid_begin_time) {
        this.valid_begin_time = valid_begin_time;
        return this;
    }

    public Long getValid_end_time() {
        return valid_end_time;
    }

    public ModifyPromotionActivityImActivity setValid_end_time(Long valid_end_time) {
        this.valid_end_time = valid_end_time;
        return this;
    }


    public static final class ModifyPromotionActivityImActivityBuilder {
        private String notice_text;
        private Long valid_begin_time;
        private Long valid_end_time;

        private ModifyPromotionActivityImActivityBuilder() {
        }

        public ModifyPromotionActivityImActivityBuilder noticeText(String noticeText) {
            this.notice_text = noticeText;
            return this;
        }

        public ModifyPromotionActivityImActivityBuilder validBeginTime(Long validBeginTime) {
            this.valid_begin_time = validBeginTime;
            return this;
        }

        public ModifyPromotionActivityImActivityBuilder validEndTime(Long validEndTime) {
            this.valid_end_time = validEndTime;
            return this;
        }

        public ModifyPromotionActivityImActivity build() {
            ModifyPromotionActivityImActivity modifyPromotionActivityImActivity = new ModifyPromotionActivityImActivity();
            modifyPromotionActivityImActivity.setNotice_text(notice_text);
            modifyPromotionActivityImActivity.setValid_begin_time(valid_begin_time);
            modifyPromotionActivityImActivity.setValid_end_time(valid_end_time);
            return modifyPromotionActivityImActivity;
        }
    }
}
