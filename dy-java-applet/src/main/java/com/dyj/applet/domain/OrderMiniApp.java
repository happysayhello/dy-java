package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-06 14:23
 **/
public class OrderMiniApp {

    /**
     * 小程序的appid
     */
    private String app_id;
    /**
     * 用户的抖音小程序openid
     */
    private String user_open_id;
    /**
     * 订单的细节，不同的订单业务有不同的结构体，请具体询问业务方字段结构
     */
    private String order_detail;
    /**
     * 订单类型-
     * 201 预约点餐订单,
     * 202 餐厅预定订单,
     * 203 餐厅排队订单,
     * 9001 景区门票订单,
     * 9101 团购券订单,
     * 9201 在线预约订单,
     * 9301 外卖订单,
     * 140 住宿订单,
     * 200 预售券订单
     */
    private Integer order_type;

    /**
     * 发送请求的时间，精确到毫秒
     */
    private Long date_time;

    public static OrderMiniAppBuilder builder() {
        return new OrderMiniAppBuilder();
    }

    public static class OrderMiniAppBuilder {
        private String appId;
        private String userOpenId;
        private String orderDetail;
        private Integer orderType;
        private Long dateTime;

        public OrderMiniAppBuilder appId(String appId) {
            this.appId = appId;
            return this;
        }
        public OrderMiniAppBuilder userOpenId(String userOpenId) {
            this.userOpenId = userOpenId;
            return this;
        }
        public OrderMiniAppBuilder orderDetail(String orderDetail) {
            this.orderDetail = orderDetail;
            return this;
        }
        public OrderMiniAppBuilder orderType(Integer orderType) {
            this.orderType = orderType;
            return this;
        }
        public OrderMiniAppBuilder dateTime(Long dateTime) {
            this.dateTime = dateTime;
            return this;
        }

        public OrderMiniApp build() {
            OrderMiniApp orderMiniApp = new OrderMiniApp();
            orderMiniApp.setApp_id(appId);
            orderMiniApp.setUser_open_id(userOpenId);
            orderMiniApp.setOrder_detail(orderDetail);
            orderMiniApp.setOrder_type(orderType);
            orderMiniApp.setDate_time(dateTime);
            return orderMiniApp;
        }
    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getUser_open_id() {
        return user_open_id;
    }

    public void setUser_open_id(String user_open_id) {
        this.user_open_id = user_open_id;
    }

    public String getOrder_detail() {
        return order_detail;
    }

    public void setOrder_detail(String order_detail) {
        this.order_detail = order_detail;
    }

    public Integer getOrder_type() {
        return order_type;
    }

    public void setOrder_type(Integer order_type) {
        this.order_type = order_type;
    }

    public Long getDate_time() {
        return date_time;
    }

    public void setDate_time(Long date_time) {
        this.date_time = date_time;
    }
}
