package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.SendCouponToDesignatedUserCouponInfo;

/**
 * 开发者接口发券返回值
 */
public class SendCouponToDesignatedUserVo {

    private SendCouponToDesignatedUserCouponInfo coupon_info;

    public SendCouponToDesignatedUserCouponInfo getCoupon_info() {
        return coupon_info;
    }

    public SendCouponToDesignatedUserVo setCoupon_info(SendCouponToDesignatedUserCouponInfo coupon_info) {
        this.coupon_info = coupon_info;
        return this;
    }
}
